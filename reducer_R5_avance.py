#!/usr/bin/env python3
import sys
from functools import reduce

def mean(l):
    return reduce(lambda x1,x2: x1+x2,l) / len(l)

customers = {}
orders = {}


# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split("\t")

    # si le nom n'est pas indiqué  
    if words[1] == "-" and words[2] != "-":
        if words[0] not in orders :
            orders [words[0]] = list()

        orders [words[0]].append(int(words[2]))

    elif words[1] != "-" and words[2] == "-" :
        if words[0] not in customers : 
            customers[words[0]] = words[1]

for id_customer in customers.keys() :
    if id_customer in orders:
        print('{}\t{}'.format(customers[id_customer],mean(orders[id_customer]) ) )

"""
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.10.0.jar -input data/Customer.dat -input data/Order.dat -output output_R5_avance/ -file src/Map_R4_avance.py -file src/reducer_R5_avance.py  -mapper Map_R4_avance.py -reducer reducer_R5_avance.py
"""