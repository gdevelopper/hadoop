#!/usr/bin/env python3
#mapper_R0.py
import sys


# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split(",")

    # à ce niveau j'applique la close where pour selectioner que les entrées qui ont leur total > 1000
    if int(words[1]) > 1000: 
        # à ce niveau là je retourne l'id correspondant
        print('{}'.format(int(words[0]) ) )
   

"""
Exo 5.2 :
Remarque : il faut se placer dans le répertoir exo2

Voici la commande d'éxécution :
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.10.0.jar -input data/Order.dat -output output_R0/ -file src/Map_R0.py  -mapper Map_R0.py

Voici le retour dans la console :
 File System Counters
    FILE: Number of bytes read=4715152603
    FILE: Number of bytes written=4023052462
    FILE: Number of read operations=0
    FILE: Number of large read operations=0
    FILE: Number of write operations=0  
Map-Reduce Framework
    Map input records=35563004
    Map output records=35491925
    Map output bytes=308333276
    Map output materialized bytes=379317210
    Input split bytes=1652
    Combine input records=0
    Combine output records=0
    Reduce input groups=3556157
    Reduce shuffle bytes=379317210
    Reduce input records=35491925
    Reduce output records=35491925
    Spilled Records=70983850
    Shuffled Maps =14
    Failed Shuffles=0
    Merged Map outputs=14
    GC time elapsed (ms)=345
    Total committed heap usage (bytes)=6036652032
Shuffle Errors
    BAD_ID=0
    CONNECTION=0
    IO_ERROR=0
    WRONG_LENGTH=0
    WRONG_MAP=0
    WRONG_REDUCE=0
File Input Format Counters
    Bytes Read=451256077
File Output Format Counters
    Bytes Written=310742140
20/01/27 21:34:28 INFO streaming.StreamJob: Output directory: output_R0/   
"""