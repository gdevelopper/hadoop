#!/usr/bin/env python3
#mapper_R0.py
import sys

# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split(",")

    # à ce niveau j'applique la close where pour selectioner que les entrées qui ont leur total > 1000
    # puis je vérifie s'il n'a pas encore été ajouté dans la list des ids pour la close DISTINCT  
    if int(words[1]) > 1000 : 
        # à ce niveau là je retourne l'id correspondant
        print('{}'.format(int(words[0]) ) )
   

"""
Exo 5.3  
Le mapper est pareil que celui de la quetion 5.2.
A ce niveau la on a besoin d'un reducer et d'un combiner (le code du reducer est le même que le reducer); c'est par soucie de performance

Exo 5.4
le code du combiner sera la même chose que le reducer car il va limiter les echanges réseau lors du shufel

Exo 5.5 : il faut exécuter le Map_R11 et comparer avec la trace ci-dessous plus précisément dans la ligne "GC time elapsed (ms)"

Remarque : il faut se placer dans le répertoir exo2

Voici la commande d'éxécution :
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.10.0.jar -input data/Order.dat -output output_R1/ -file src/Map_R1.py -file src/reducer_R1.py  -mapper Map_R1.py -reducer reducer_R1.py -combiner reducer_R1.py

Voici le retour dans la console avec le reducer:
    20/01/27 22:48:44 INFO mapreduce.Job: Counters: 30
        File System Counters
                FILE: Number of bytes read=4274835316
                FILE: Number of bytes written=2438262210
                FILE: Number of read operations=0
                FILE: Number of large read operations=0
                FILE: Number of write operations=0
        Map-Reduce Framework
                Map input records=35563004
                Map output records=35491925
                Map output bytes=308333276
                Map output materialized bytes=269559266
                Input split bytes=1652
                Combine input records=59710171
                Combine output records=28777962
                Reduce input groups=3556157
                Reduce shuffle bytes=269559266
                Reduce input records=4559716
                Reduce output records=3556157
                Spilled Records=29781585
                Shuffled Maps =14
                Failed Shuffles=0
                Merged Map outputs=14
                GC time elapsed (ms)=423
                Total committed heap usage (bytes)=6008340480
        Shuffle Errors
                BAD_ID=0
                CONNECTION=0
                IO_ERROR=0
                WRONG_LENGTH=0
                WRONG_MAP=0
                WRONG_REDUCE=0
        File Input Format Counters
                Bytes Read=451256077
        File Output Format Counters
                Bytes Written=31135725
20/01/27 22:48:44 INFO streaming.StreamJob: Output directory: output_R1/
"""