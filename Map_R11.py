#!/usr/bin/env python3

import sys

ids  = set() 
# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split(",")

    # à ce niveau j'applique la close where pour selectioner que les entrées qui ont leur total > 1000
    # puis je vérifie s'il n'a pas encore été ajouté dans la list des ids pour la close DISTINCT  
    if int(words[1]) > 1000 and words[0] not in ids: 
        # à ce niveau là je retourne l'id correspondant
        ids.add(words[0])
        print('{}'.format(int(words[0]) ) )