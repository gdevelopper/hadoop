#!/usr/bin/env python3
import sys

customers = {}
orders = {}


# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split('\t')

    # si le nom n'est pas indiqué  
    if words[1] == "-" and words[2] != "-":
        if words[0] not in orders :
            orders [words[0]] = list()
        orders [words[0]].append(int(words[2]))

    elif words[1] != "-" and words[2] == "-" :
        if words[0] not in customers : 
            customers[words[0]] = words[1]

for id_customer in customers.keys() :
    if id_customer in orders:
        for total in orders[id_customer] :
            print('{}\t{}'.format(customers[id_customer],total ) )