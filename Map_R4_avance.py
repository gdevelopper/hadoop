#!/usr/bin/env python3
import sys

# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split(",")

    id_customer = "-"
    nom = "-"
    total = "-"

    # traitement des lignes de customer 
    if len(words) == 3 :
        id_customer = words[0]
        nom = words[2]

    # traitement des lignes d'order
    else :
        id_customer = words[0]
        total = words[1]
    
    print( '{}\t{}\t{}'.format(id_customer, nom, total) )

"""
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.10.0.jar -input data/Customer.dat -input data/Order.dat -output output_R4_avance/ -file src/reducer_R4_avance.py -file src/Map_R4_avance.py   -mapper Map_R4_avance.py -reducer reducer_R4_avance.py
"""