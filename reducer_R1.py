#!/usr/bin/env python3
#mapper_URL.py
import sys

ids =set() # à ce niveau j'utilise set car x in set est de completixité 1 dans la plus part des temps 

#partitioner
for line in sys.stdin:
    id =line.strip()

    if id not in ids:
        ids.add(id)
        print('{}'.format(id))