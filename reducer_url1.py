#!/usr/bin/env python3
#mapper_URL.py
import sys
import numpy as np
from functools import reduce 
times ={}

#partitioner
for line in sys.stdin:
    line=line.strip()
    items=line.split()

    if items[0] in times:
        times[items[0]].append((int(items[1]),int(items[2])))
    else:
        times[items[0]]=[]
        times[items[0]].append((int(items[1]),int(items[2])))

#calculer la moyenne (le reducer)
for url in times.keys():
    res = reduce(lambda a,c : (a[0]+c[0],a[1]+c[1]), times[url])
    print('{}\t{}'.format(url, res[0]/res[1]));