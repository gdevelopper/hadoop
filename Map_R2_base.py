#!/usr/bin/env python3
import sys
from datetime import datetime

# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split(",")

    print('{}\t{}'.format(datetime.strptime(words[1],"%d/%m/%Y").month,words[0] ) )


"""
Voici la commande d'éxécution pour la méthode de base:
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.10.0.jar -input data/Customer.dat -output output_R2_base/ -file src/Map_R2_base.py -file src/reducer_R2_base.py  -mapper Map_R2_base.py -reducer reducer_R2_base.py 

Pour ce qui concerne la méthode avancé :
j'ai dévéloppé un nouvel reducer (reducer_R2_avance.py) et rajouter un combiner qui a le meme code que le reducer de base voici la commande d'éxécution :
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.10.0.jar -input data/Customer.dat -output output_R2_avance/ -file src/Map_R2_base.py -file src/reducer_R2_avance.py -file src/combiner_R2_avance.py  -mapper Map_R2_base.py -reducer reducer_R2_avance.py -combiner combiner_R2_avance.py 
"""