#!/usr/bin/env python3
#mapper_URL.py
import sys

months = {} 

#partitioner
for line in sys.stdin:
    line = line.strip()
    words = line.split()

    if words[0] not in months:
        months[words[0]] = 1
    else :
        months[words[0]] = months[words[0]] + 1

for month,count in months.items():
    print('{}\t{}'.format(month,count))