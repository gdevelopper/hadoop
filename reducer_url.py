#!/usr/bin/env python3
#mapper_URL.py
import sys
import numpy as np
times ={}

#partitioner
for line in sys.stdin:
    line=line.strip()
    items=line.split()

    if items[0] in times:
        times[items[0]].append(int(items[1]))
    else:
        times[items[0]]=[]
        times[items[0]].append(int(items[1]))

#calculer la moyenne (le reducer)
for url in times.keys():
    ave_time = np.mean(times[url])
    print('{}\t{}'.format(url, ave_time))