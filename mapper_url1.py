#!/usr/bin/env python3
#mapper_URL.py
import sys


# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split("\t")
    print('{}\t{}\t1'.format(words[0], int(words[1]) ) )