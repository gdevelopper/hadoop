#!/usr/bin/env python3
import sys

customers = {}
orders = {}


# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split(",")

    # traitement des lignes de customer 
    if len(words) == 3 :
        if words[0] not in customers :
            customers[words[0]] = words[2]
    # traitement des lignes de order
    else :
        if words[0] not in orders:
            orders[words[0]] = list()
            orders[words[0]].append(int(words[1]))


for id_customer in customers.keys() :
    if id_customer in orders:
        for total in orders[id_customer] :
            print('{}\t{}'.format(customers[id_customer],total ) )

"""
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.10.0.jar -input data/Customer.dat -input data/Order.dat -output output_R4_base/ -file src/Map_R4_base.py   -mapper Map_R4_base.py
"""