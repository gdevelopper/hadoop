#!/usr/bin/env python3
import sys

# input comes from STDIN (standard input)
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split(",")

    print('{}\t{}\t{}'.format(words[2],words[0],words[1] ) )


"""
commande d'execution
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.10.0.jar -input data/Customer.dat -output output_R3_base/ -file src/Map_R3_base.py   -mapper Map_R3_base.py
"""